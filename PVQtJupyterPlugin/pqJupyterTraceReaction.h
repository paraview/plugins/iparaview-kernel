#ifndef pqJupyterTraceReactions_h
#define pqJupyterTraceReactions_h

#include <pqReaction.h>

#include <QScopedPointer>

class pqJupyterTraceReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  pqJupyterTraceReaction(QAction* p);
  ~pqJupyterTraceReaction() override;

protected slots:
  void onTriggered() override;
  void onStandardAction();

  void updateAction();
  void initialize();
  void sendToJupyter(QString message);

private:
  class pqInternals;
  QScopedPointer<pqInternals> Internals;

  // The jupyter kernel is always the first to connect that is the client Id 1
  static constexpr int JUPYTER_CLIENT_ID{ 1 };
};

#endif
